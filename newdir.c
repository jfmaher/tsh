/* newdir.c */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
    mkdir(argv[1], S_IRWXU);
    return(0);
}
