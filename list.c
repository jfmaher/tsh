/* list.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    char path[40] = {};
    getcwd(path, sizeof(path));
    DIR *dir;
    dir = opendir(path);

    struct dirent *ent;
    
    if(argv[1] != NULL && strcmp(argv[1], "-d") == 0)
    {
        if(dir != NULL)
        {
            while((ent = readdir(dir)) != NULL)
            {
                if(strcmp(ent->d_name,".") != 0 && strcmp(ent->d_name,"..") != 0)
                {
                    printf("%s\n", ent->d_name);
                }
            }
            closedir(dir);
        }
    }
    else
    {
        if(dir != NULL)
        {
            while((ent = readdir(dir)) != NULL)
            {
                if(ent->d_type != DT_DIR)
                {
                    printf("%s\n", ent->d_name);
                }
            }
            closedir(dir);
        }
    }
    return(0);
}
