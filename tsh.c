/* tsh.c */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#define FGETS_MAX 40
#define MAX_ARRAY 200
#define MAX_ELEM 20

int main()
{

    char input[MAX_ARRAY] = "\0", ultimatePath[MAX_ARRAY] = {};

    getcwd(ultimatePath, sizeof(ultimatePath));
        
    while(1)
    {
        char path[MAX_ARRAY] = {}, *argv[MAX_ELEM] = {""}, *token = "", temp[MAX_ARRAY] = {};
        printf("tsh> ");
        fgets(input, FGETS_MAX, stdin);
        scanf("%[^\n]*s", input);
        token = strtok(input, " ");
        
        int i = 0;

        while(token != NULL)
        {
            argv[i++] = token;
            token = strtok(NULL, " ");
            strcat(token,"\0");
        }

        argv[i++] = ultimatePath; 
        getcwd(path, sizeof(path));
        argv[i] = path;

        if(strcmp(argv[0], "exit") == 0)
        {
            break;
        }
        else if(strcmp(argv[0], "cd") == 0)
        {
            chdir(argv[1]);
        }
        else if(strcmp(argv[0], "cdir") == 0)
        {
            getcwd(path, sizeof(path));
            printf("Repertoire courant : %s\n", path);
            strcpy(path, "");
        }
        else if(strcmp(argv[0], "\n") !=0)
        {
            strcpy(temp, ultimatePath);
            strcat(temp, "/inf3172/bin/");
            strcat(temp, argv[0]);
            argv[0] = temp;

            pid_t pid, tpid;
            int status;

            pid = fork();
        
            if(pid == 0)
            {
                execvp(argv[0], argv);
                perror("tsh");
                exit(0);
            }
            else
            {
                do
                {
                    tpid = wait(&status);
                    if(tpid != pid)
                    {
                        kill(tpid, 0);
                    }
                } while(tpid != pid);
            }
        }
    }

    return(0);
}
