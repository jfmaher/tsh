/* fin.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#define MAX_LINE 300
#define MAX_ARRAY 200

void finFile(char *path, int nbLines)
{
    int i = 0;
    FILE* file = fopen(path, "r");
    FILE* fileDup = fopen(path, "r");
    char line[MAX_LINE];

    printf("OK avant while dans finFile, nbFiles : %d\n", nbLines);

    while(fgets(line, sizeof(line), file))
    {
        i++;
    }

    fclose(file);

    while(fgets(line, sizeof(line), fileDup))
    {
        i--;
        if(i < nbLines)
        {
            printf("%s", line);
        }
    }
    
    printf("\n");
    fclose(fileDup);
}

int main(int argc, char *argv[])
{
    printf("%s\n", argv[0]);
    printf("%s\n", argv[1]);
    printf("%s\n", argv[2]);
    printf("%s\n", argv[3]);
    printf("%s\n", argv[4]);
    
    char temp[MAX_ARRAY] = {}, c;
    int i = atoi(argv[1]);

    c = argv[2][0];
    if(c == '/')
    {
        finFile(argv[2], i);
    }
    else
    {
        strcpy(temp, argv[4]);
        strcat(temp, "/");
        strcat(temp, argv[2]);
        finFile(temp, i);
    }
    return(0);
}
