/* rmall.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>

#define MAX_ARRAY 200

void removeDir(char *path)
{
    DIR *dir;
    dir = opendir(path);

    struct dirent *ent;
    
    if(dir != NULL)
    {
        while((ent = readdir(dir)) != NULL)
        {
            char temp[MAX_ARRAY] = {};
            
            if(strcmp(ent->d_name,".") != 0 && strcmp(ent->d_name,"..") != 0)
            {
                strcpy(temp, path);
                strcat(temp, "/");
                strcat(temp, ent->d_name);
                
                if(ent->d_type == DT_DIR)
                {
                    removeDir(temp);
                }
                remove(temp);
            }
        }
        remove(path);
        closedir(dir);
    }
}

int main(int argc, char *argv[])
{
    char temp[MAX_ARRAY] = {}, c;

    c = argv[1][0];
    if(c == '/')
    {
        removeDir(argv[1]);
    }
    else
    {
        strcpy(temp, argv[3]);
        strcat(temp, "/");
        strcat(temp, argv[1]);
        removeDir(temp);
    }
    return(0);
}
