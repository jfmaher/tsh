/* size.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MAX_ARRAY 200

void sizeDir(char *path, int *info)
{
    DIR *dir;
    dir = opendir(path);

    struct dirent *ent;
    
    if(dir != NULL)
    {
        while((ent = readdir(dir)) != NULL)
        {
            char temp[MAX_ARRAY] = {};
            struct stat st;
            
            if(strncmp(ent->d_name,".", 1) != 0 && strcmp(ent->d_name,"..") != 0)
            {
                strcpy(temp, path);
                strcat(temp, "/");
                strcat(temp, ent->d_name);

                if(ent->d_type == DT_DIR)
                {
                    sizeDir(temp, info);
                }
                else
                {
                    info[0]++;
                    stat(temp, &st);
                    info[1] += st.st_size;
                }
            }
        }
        closedir(dir);
    }
}

int main(int argc, char *argv[])
{
    char temp[MAX_ARRAY] = {}, c;
    int info[2] = {0};

    c = argv[1][0];
    if(c == '/')
    {
        sizeDir(argv[1], info);
    }
    else
    {
        strcpy(temp, argv[3]);
        strcat(temp, "/");
        strcat(temp, argv[1]);
        sizeDir(temp, info);
    }

    printf("Le dossier contient %d fichier(s) pour une taille totale de %d.\n", info[0], info[1]);
    return(0);
}
