all: mkdir tsh new list rmall newdir size fin
	gcc -o tsh tsh.c -Wall

mkdir: 
	mkdir -p inf3172/bin

tsh: tsh.c
	gcc -o inf3172/bin/tsh tsh.c -Wall

new: new.c
	gcc -o inf3172/bin/new new.c -Wall

list: list.c
	gcc -o inf3172/bin/list list.c -Wall

rmall: rmall.c
	gcc -o inf3172/bin/rmall rmall.c -Wall

newdir: newdir.c
	gcc -o inf3172/bin/newdir newdir.c -Wall

size: size.c
	gcc -o inf3172/bin/size size.c -Wall

fin: fin.c
	gcc -o inf3172/bin/fin fin.c -Wall

.PHONY: clean

clean:
	rm -f tsh *.o
	rm -f inf3172/bin/*
	rmdir inf3172/bin/
	rmdir inf3172/
